package com.hzf.drivingtour.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;

import org.junit.Before;
import org.junit.Test;

import com.hzf.drivingtour.entity.ERoute;
import com.hzf.drivingtour.map.MapUtil;

public class KMLParseUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testParseKML() {
		try {
			FileInputStream fis = new FileInputStream("..\\DrivingTour\\assets\\route1.kml");
			ERoute vo = MapUtil.parseKML(fis);
			assertNotNull(vo);
			System.err.println("title:" + vo.getTitle() + "--Description:" + vo.getDescription());
			System.err.println("start:" + vo.getStartX() + "--" + vo.getStartY());
			System.err.println("end:" + vo.getEndX() + "--" + vo.getEndY());
			String[] ps = vo.getPoints().split(",");
			assertEquals(ps.length % 2, 0);
			System.err.println("Points.length:" + ps.length / 2);
			for (int i = 0; i < ps.length; i += 2) {
				if (i == 0) {
					assertEquals(vo.getStartX() + "", ps[i]);
					assertEquals(vo.getStartY() + "", ps[i + 1]);
				} else if (i == ps.length - 2) {
					assertEquals(vo.getEndX() + "", ps[i]);
					assertEquals(vo.getEndY() + "", ps[i + 1]);
				}
				System.err.println(ps[i] + "--" + ps[i + 1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
