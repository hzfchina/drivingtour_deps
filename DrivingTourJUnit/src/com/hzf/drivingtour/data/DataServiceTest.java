package com.hzf.drivingtour.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.baidu.mapapi.GeoPoint;
import com.hzf.drivingtour.entity.ELocation;
import com.hzf.drivingtour.entity.ERoute;
import com.hzf.drivingtour.map.MapUtil;
import com.xtremelabs.robolectric.Robolectric;
import com.xtremelabs.robolectric.RobolectricTestRunner;
import com.xtremelabs.robolectric.shadows.ShadowSQLiteDatabase;
import com.xtremelabs.robolectric.util.DatabaseConfig.UsingDatabaseMap;
import com.xtremelabs.robolectric.util.SQLiteMap;

@UsingDatabaseMap(SQLiteMap.class)
@RunWith(RobolectricTestRunner.class)
public class DataServiceTest {
	protected SQLiteDatabase db;
	protected ShadowSQLiteDatabase shDatabase;
	private DataService service;

	@Before
	public void setUp() throws Exception {
		Context c = new Activity();
		db = SQLiteDatabase.openDatabase(".\\temp\\db\\maindb", null, 0);
		shDatabase = Robolectric.shadowOf(db);
		service = new DataService(c);
		service.dbOpenHelper.onCreate(db);
	}

	@After
	public void tearDown() throws Exception {
		db.close();
	}

	@Test
	public void testSearchNearLocations() {
		try {
			//import route1.kml into Route table
			InputStream is = new FileInputStream("..\\DrivingTour\\assets\\route1.kml");
			ERoute route = MapUtil.parseKML(is);
			assertNotNull(route);
			assertEquals(route.getTitle(), "上班路线");
			assertTrue(route.getDescription() != null && route.getDescription().trim().length() > 0);
			assertEquals(route.getStartX(), 102669277);
			assertEquals(route.getStartY(), 25067217);
			assertEquals(route.getEndX(), 102782088);
			assertEquals(route.getEndY(), 24983731);
			assertTrue(route.getPoints() != null && route.getPoints().trim().length() > 0);
			MainDatabaseHelper.saveEntity(db, route);

			//import all Rotue points into Location table for test
			ELocation location = new ELocation();
			location.setTitle(route.getTitle() + "-起点");
			location.setGeoPoint(route.getStartGeoPoint());
			MainDatabaseHelper.saveEntity(db, location);

			location = new ELocation();
			location.setTitle(route.getTitle() + "-终点");
			location.setGeoPoint(route.getEndGeoPoint());
			MainDatabaseHelper.saveEntity(db, location);
			String[] ponits = route.getPoints().split(",");
			for (int i = 0; i < ponits.length; i += 2) {
				location = new ELocation();
				location.setTitle(route.getTitle() + "--" + i);
				location.setType(i % 8);
				location.setPointX(Integer.parseInt(ponits[i]));
				location.setPointY(Integer.parseInt(ponits[i + 1]));
				MainDatabaseHelper.saveEntity(db, location);
			}

			GeoPoint p = new GeoPoint(102669277, 25067217);
			Integer[] types = new Integer[] { ELocation.Type_UNKNOWN, ELocation.Type_GasStation, ELocation.Type_Hotle };
			assertEquals(ELocation.Column_Types.length, ELocation.Columns.length);
			assertEquals(new ELocation().getColumn_Types().length, ELocation.Columns.length);

			List<ELocation> locations = service.searchNearLocations(p, 300, types);
			assertNotNull(locations);
			assertEquals(locations.size(), 100);
			for (ELocation l : locations) {
				Log.d(this.getClass().getSimpleName(), "distance=" + MapUtil.distance(p, l.getGeoPoint()));
				assertTrue(Arrays.binarySearch(types, l.getType()) >= 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
	}

}
