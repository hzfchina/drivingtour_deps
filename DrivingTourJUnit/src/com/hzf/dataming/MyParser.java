package com.hzf.dataming;

import org.htmlparser.Parser;  
import org.htmlparser.PrototypicalNodeFactory;  
import org.htmlparser.lexer.Lexer;  
import org.htmlparser.util.ParserException;
import org.htmlparser.util.ParserFeedback;  
/**  
 * @（#）:MyParser.java  
 * @description:  继承自Parser的类,对Parser进行扩展 
 * @author:  py  2010-12-9  
 */  
public class MyParser extends Parser {  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static PrototypicalNodeFactory factory = null;  
    //注册自定义标签  
    static{  
        factory = new PrototypicalNodeFactory();  
        factory.registerTag(new StrongTag());  
        factory.registerTag(new SpanTag());  
    }  
    public MyParser(){  
        super();  
        setNodeFactory(factory);  
    }  
      
    public MyParser(Lexer lexer, ParserFeedback fb) {  
        super(lexer, fb);  
        setNodeFactory(factory);  
    }

	public MyParser(String resource)  throws ParserException{
		 super(resource, STDOUT);  
		 setNodeFactory(factory);  
	}  
//    public Parser(String resource)
//            throws ParserException
//        {
//            this(resource, STDOUT);
//        }
}  
