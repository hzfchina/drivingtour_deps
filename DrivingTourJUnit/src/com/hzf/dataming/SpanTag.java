package com.hzf.dataming;

import org.htmlparser.tags.CompositeTag;

public class SpanTag extends CompositeTag {
	private static final long serialVersionUID = -9038383217802935072L;
	private static final String mIds[] = {
			"SPAN"
	};
	private static final String mEndTagEnders[] = {
			"SPAN"
	};

	public SpanTag()
	{
	}

	@Override
	public String[] getIds()
	{
		return mIds;
	}

	@Override
	public String[] getEndTagEnders()
	{
		return mEndTagEnders;
	}
}