package com.hzf.dataming;

import java.util.List;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.ParagraphTag;
import org.htmlparser.tags.TableRow;
import org.htmlparser.tags.TableTag;
import org.htmlparser.tags.TitleTag;
import org.htmlparser.util.NodeList;

public class baseMing {

	List<dataPo> list;

	//导数据出来
	public List<dataPo> catchData(String html, String[] keywords) {

		return list;
	}

	//放入sqllite数据库中
	public boolean putList() {

		return false;
	}

	//根据不同的网页标签来截取内容
	public static NodeList getTag(String html, int tagType) throws Exception
	{
		Parser parser = new Parser(html);
		MyParser myParser = new MyParser(html);
		NodeList nodeList = new NodeList();

		switch (tagType)
		{
		case 0:
			nodeList = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = -5796447805671468697L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof LinkTag) {
						return true;
					}
					return false;
				}
			});
			break;
		case 1:
			nodeList = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = -6190418007004371337L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof TableTag) {
						return true;
					}
					return false;
				}
			});
			break;
		case 2:
			nodeList = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = -2005130949717872001L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof Div) {
						return true;
					}
					return false;
				}
			});
			break;
		case 3:
			nodeList = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = 6533291266620634556L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof TableRow) {
						return true;
					}
					return false;
				}
			});
			break;
		case 4:
			nodeList = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = 4570169669976390681L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof ParagraphTag) {
						return true;
					}
					return false;
				}
			});
			break;
		case 5:
			nodeList = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = 7156739747955305685L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof TitleTag) {
						return true;
					}
					return false;
				}
			});
			break;
		case 6:
			nodeList = myParser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = 1L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof SpanTag) {
						return true;
					}
					return false;
				}
			});
			break;
		case 7:
			nodeList = myParser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = 1L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof StrongTag) {
						return true;
					}
					return false;
				}
			});
			break;
		case 8:
			nodeList = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				private static final long serialVersionUID = 1L;

				// 实现该方法,用以过滤标签
				@Override
				public boolean accept(Node node)
				{
					if (node instanceof ImageTag) {
						return true;
					}
					return false;
				}
			});
			break;
		default:
		}
		return nodeList;
	}

}
