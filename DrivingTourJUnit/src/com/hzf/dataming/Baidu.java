package com.hzf.dataming;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.htmlparser.tags.Div;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.ParagraphTag;
import org.htmlparser.tags.TitleTag;
import org.htmlparser.util.NodeList;

public class Baidu {

	//	String parentHtml = "http://lvyou.55tuan.com/kunming-1-0-0-1";
	String parentHtml = "http://lvyou.baidu.com";
	List<dataPo> list;
	List<String> listTemp;

	//导数据出来  <div class="scene-pic-container nslog-area">
	//tag:class   keyword: scene-pic-container nslog-area
	public List<String> catchData1(String html, String tag, String keyword) throws Exception {
		NodeList nodeList = baseMing.getTag(html, 2);
		List<String> listTemp = new ArrayList<String>();
		for (int i = 0; i < nodeList.size(); i++)
		{
			Div div = (Div) nodeList.elementAt(i);
			if (div.getAttribute(tag) != null && div.getAttribute(tag).equalsIgnoreCase(keyword))
			{
				//	    		logger.debug("end getContent"+i);
				//logger.debug("n.getChildrenHTML() ="+n.getChildrenHTML());
				listTemp.add(div.getChildrenHTML());
			}
		}
		return listTemp;
	}

	//	public List<dataPo> catchData2(List<String> listTemp) throws Exception{
	public List<String> catchData2(List<String> listTemp) throws Exception {
		//先做一个循环，取得每一个子网站
		//处理A标签
		List<String> AList = new ArrayList<String>();
		//		for(String htmltemp: listTemp){
		int i = 0;
		for (; i < listTemp.size(); i = i + 3) {
			NodeList nodeList = baseMing.getTag(listTemp.get(i), 0);
			LinkTag lt = (LinkTag) nodeList.elementAt(0);

			//			 NodeList  imgList  =  baseMing.getTag(listTemp.get(i), 8);
			//			 ImageTag it = (ImageTag) imgList.elementAt(0);//ParagraphTag
			//			 it.getAttribute("src");//这就是图片的地址

			if (lt.getLink().startsWith("/")) {
				AList.add(lt.getLink());
			}
			//			 System.out.println(lt.getLink());
		}
		return AList;
	}

	//子网站中的操作:在子网站中取得对应的字段中的数据
	//http://lvyou.55tuan.com/goods-2b5c79d8a939ed0a.html
	public List<dataPo> catchData3(List<String> AList) throws Exception {
		if (AList.size() <= 0) {
			return null;
		}
		List<dataPo> dplist = new ArrayList<dataPo>();
		for (String ahtml : AList) {
			dataPo dp = new dataPo();
			String childHtml = parentHtml + ahtml;
			//name
			NodeList titlelist = baseMing.getTag(childHtml, 5);
			TitleTag title = (TitleTag) titlelist.elementAt(0);
			dp.setName(title.getChildrenHTML());

			//description: 第一个 text-p text-desc-p
			NodeList plist = baseMing.getTag(childHtml, 4);
			for (int j = 0; j < plist.size(); j++)
			{
				ParagraphTag p = (ParagraphTag) plist.elementAt(j);
				if (p.getAttribute("class") != null && p.getAttribute("class").equalsIgnoreCase("text-p text-desc-p"))
				{
					dp.setDescription(p.getChildrenHTML());
					break;
				}
			}

			//现在做div
			NodeList divlist = baseMing.getTag(childHtml, 2);
			for (int i = 0; i < divlist.size(); i++)
			{
				Div div = (Div) divlist.elementAt(i);
				//address
				if (div.getAttribute("class") != null
						&& div.getAttribute("class").equalsIgnoreCase("wrap address-wrap"))
				{
					NodeList spList = baseMing.getTag(div.getChildrenHTML(), 6);
					SpanTag sp = (SpanTag) spList.elementAt(1);//SpanTag
					dp.setAddress(sp.getChildrenHTML());
				}
				//phone
				if (div.getAttribute("class") != null && div.getAttribute("class").equalsIgnoreCase("wrap phone-wrap"))
				{
					NodeList spList = baseMing.getTag(div.getChildrenHTML(), 6);
					SpanTag sp = (SpanTag) spList.elementAt(1);//SpanTag
					dp.setPhone(sp.getChildrenHTML());
				}
				//price 插入用comment字段
				if (div.getAttribute("class") != null && div.getAttribute("class").equalsIgnoreCase("val price-value"))
				{
					NodeList pList = baseMing.getTag(div.getChildrenHTML(), 4);
					ParagraphTag p = (ParagraphTag) pList.elementAt(0);//ParagraphTag
					dp.setComment(p.getChildrenHTML());
				}
				//图片
				if (div.getAttribute("class") != null
						&& div.getAttribute("class").equalsIgnoreCase("center-pic-link nslog-area"))
				{
					System.out.println("imgTag进来了");
					NodeList imgList = baseMing.getTag(div.getChildrenHTML(), 8);
					System.out.println(div.getTagBegin());
					//			    		  Div div1 = (Div)pList.elementAt(0);
					ImageTag it = (ImageTag) imgList.elementAt(0);//ParagraphTag
					it.getAttribute("src");//这就是图片的地址
					System.out.println(it.getAttribute("src"));

					URL url = new URL(it.getAttribute("src"));

					Date date1 = new Date();
					SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");
					String str1 = sdf1.format(date1);
					File outFile = new File("d:/work/hzf/map1/" + str1 + ".jpg");
					OutputStream os = new FileOutputStream(outFile);
					InputStream is = url.openStream();
					byte[] buff = new byte[1024];
					while (true) {
						int readed = is.read(buff);
						if (readed == -1) {
							break;
						}
						byte[] temp = new byte[readed];
						System.arraycopy(buff, 0, temp, 0, readed);
						os.write(temp);
					}
					is.close();
					os.close();

				}
			}
			dplist.add(dp);
		}

		return dplist;
	}

	//在子网站list中取得对应的字段中的数据
	public List<String> catchData4(List<String> listHtml) throws Exception {
		//type :  1

		//description: 第一个 text-p text-desc-p
		//<p class="text-p text-desc-p">

		//name:第一个title   <title>石林旅游攻略_百度旅游</title>

		//address:
		//<div class="wrap address-wrap">
		//		<span class="info-tit">地址：</span>
		//		<span class="val address-value">
		//		昆明市石林彝族自治县</span>
		//		</div>

		//phone:   <div  id="xq_thisr_temp"
		//		<div class="wrap phone-wrap">
		//		<span class="info-tit">电话：</span>
		//		<span class="val phone-value">
		//		0871-7711439</span>
		//		</div>

		//comment:这个网站没有评论
		/*
		 * price:用comment代替 <div class="val price-value"> <p
		 * class="text-p text-desc-p">175元，学生票100元</p></div> </div>
		 */
		return listHtml;
	}

	//放入sqllite数据库中
	public boolean putList() {

		return false;
	}

	public static void main(String args[]) throws Exception {
		Baidu ww = new Baidu();
		String html = "http://lvyou.baidu.com/scene/allview/c45b91ca87274646403518f4#1";
		String tag = "class";
		//		String keyword ="dh_spbox";//con_box clearfix
		String keyword = "scene-pic-container nslog-area";//con_box clearfix
		List<String> listTemp = ww.catchData1(html, tag, keyword);
		ww.catchData2(listTemp);
		if (listTemp.size() > 0) {
			//			System.out.println(listTemp.get(0));
			List<dataPo> list23 = ww.catchData3(ww.catchData2(listTemp));
			for (dataPo dp : list23) {
				System.out.println(dp.getAddress());
			}
		} else {
			System.out.println("nothing");
		}

	}

}
