package com.hzf.dataming;

import org.htmlparser.tags.CompositeTag;

/**
 * @（#）:StrongTag.java
 * @description: 扩展的Strong标签
 * @author: py 2010-12-9
 */
public class StrongTag extends CompositeTag {
	private static final long serialVersionUID = 4447441840287181099L;
	private static final String mIds[] = {
			"strong"
	};
	private static final String mEndTagEnders[] = {
			"strong"
	};

	public StrongTag()
	{
	}

	@Override
	public String[] getIds()
	{
		return mIds;
	}

	@Override
	public String[] getEndTagEnders()
	{
		return mEndTagEnders;
	}
}