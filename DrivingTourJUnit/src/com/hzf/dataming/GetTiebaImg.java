package com.hzf.dataming;

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.AndFilter;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.util.NodeIterator;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

public class GetTiebaImg extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JTextField jt_id;
	private JTextField pageStart;
	private JTextField pageEnd;
	private JButton startBtn;
	private JTextArea resultArea;
	private JScrollPane jspane;
	private JPanel topPanel;
	private static PrintWriter pwriter;
	private static int imgNum;

	public GetTiebaImg()
	{
		jt_id = new JTextField("1319795036", 8);
		pageStart = new JTextField("1", 3);
		pageEnd = new JTextField("3", 3);
		JLabel label = new JLabel("从第");
		JLabel label2 = new JLabel("页  到第");
		JLabel label3 = new JLabel("页");
		JLabel label4 = new JLabel("帖子ID");
		startBtn = new JButton("开始");
		topPanel = new JPanel();
		topPanel.add(label4);
		topPanel.add(jt_id);
		topPanel.add(label);
		topPanel.add(pageStart);
		topPanel.add(label2);
		topPanel.add(pageEnd);
		topPanel.add(label3);
		topPanel.add(startBtn);
		resultArea = new JTextArea("例如http://tieba.baidu.com/p/1319795036 帖子ID填1319795036");
		jspane = new JScrollPane(resultArea);
		jspane.setSize(350, 400);
		//    this.startBtn.addActionListener(new ActionListener(this)
		//    {
		//      public void actionPerformed() {
		//        int i = Integer.valueOf(GetTiebaImg.access$0(this.this$0).getText()).intValue();
		//        int s = Integer.valueOf(GetTiebaImg.access$1(this.this$0).getText()).intValue();
		//        int e = Integer.valueOf(GetTiebaImg.access$2(this.this$0).getText()).intValue();
		//        GetTiebaImg.extractImg(i, s, e);
		//      }
		//
		//    });
		add(topPanel, "North");
		add(jspane, "Center");
		setSize(450, 500);
		setDefaultCloseOperation(3);
		setVisible(true);
	}

	public static void main(String[] args)
	{
		new GetTiebaImg();
	}

	public static void extractImg(int id, int start, int end) {
		int choice;
		Desktop d;
		try {
			System.out.println("初始化....");
			pwriter = new PrintWriter(
					new BufferedWriter(new FileWriter("c:/tieba_" + id + ".html", true)));
			pwriter.println("<html><head></head><body>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try
		{
			for (int i = start; i <= end; ++i) {
				Parser parser = new Parser("http://tieba.baidu.com/p/" + id + "?pn=" + i);
				NodeFilter filter = new AndFilter(new TagNameFilter("img"), new HasAttributeFilter("class", "BDE_Image"));
				NodeList nodes = parser.extractAllNodesThatMatch(filter);
				imgNum = nodes.size();
				if (imgNum > 0) {
					for (NodeIterator ni = nodes.elements(); ni.hasMoreNodes();) {
						pwriter.println(ni.nextNode().toHtml());
					}
				}

			}

			pwriter.println("</body></html>");
		} catch (ParserException e) {
			pwriter.println("</body></html>");

			pwriter.close();
			choice = JOptionPane.showConfirmDialog(null, "找到"
					+ imgNum + "张图片\n已保存到" + "c:/tieba_" + id + ".html" + "是否立即打开", ">..<", 0);
			if (choice != 0) {
				return;
			}
			d = Desktop.getDesktop();
			try {
				d.browse(new URI("file:///c:/tieba_" + id + ".html"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally
		{
			pwriter.close();
			choice = JOptionPane.showConfirmDialog(null, "找到"
					+ imgNum + "张图片\n已保存到" + "c:/tieba_" + id + ".html" + "是否立即打开", ">..<", 0);
			if (choice == 0) {
				d = Desktop.getDesktop();
				try {
					d.browse(new URI("file:///c:/tieba_" + id + ".html"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}