package com.hzf.dataming;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.htmlparser.tags.Div;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.ParagraphTag;
import org.htmlparser.tags.TitleTag;
import org.htmlparser.util.NodeList;

public class Wowo {

	//	String parentHtml = "http://lvyou.55tuan.com/kunming-1-0-0-1";
	String parentHtml = "http://lvyou.55tuan.com";
	List<dataPo> list;
	List<String> listTemp;

	//导数据出来  <div class="dh_spbox"> 第一步提取  窝窝团 昆明 周边游 的信息  http://lvyou.55tuan.com/kunming-1-0-0-1
	//tag:class   keyword: dh_spbox
	public List<String> catchData1(String html, String tag, String keyword) throws Exception {
		NodeList nodeList = baseMing.getTag(html, 2);
		List<String> listTemp = new ArrayList<String>();
		for (int i = 0; i < nodeList.size(); i++)
		{
			Div div = (Div) nodeList.elementAt(i);
			if (div.getAttribute(tag) != null && div.getAttribute(tag).equalsIgnoreCase(keyword))
			{
				//	    		logger.debug("end getContent"+i);
				//logger.debug("n.getChildrenHTML() ="+n.getChildrenHTML());
				listTemp.add(div.getChildrenHTML());
			}
		}
		return listTemp;
	}

	//	public List<dataPo> catchData2(List<String> listTemp) throws Exception{
	public List<String> catchData2(List<String> listTemp) throws Exception {
		//先做一个循环，取得每一个子网站
		//处理A标签
		List<String> AList = new ArrayList<String>();
		for (String htmltemp : listTemp) {
			NodeList nodeList = baseMing.getTag(htmltemp, 0);
			LinkTag lt = (LinkTag) nodeList.elementAt(0);
			if (lt.getLink().startsWith("/")) {
				AList.add(lt.getLink());
			}
			//			 System.out.println(lt.getLink());
		}
		return AList;
	}

	//子网站中的操作:在子网站中取得对应的字段中的数据
	//http://lvyou.55tuan.com/goods-2b5c79d8a939ed0a.html
	public List<dataPo> catchData3(List<String> AList) throws Exception {
		if (AList.size() <= 0) {
			return null;
		}
		List<dataPo> dplist = new ArrayList<dataPo>();
		for (String ahtml : AList) {
			dataPo dp = new dataPo();
			String childHtml = parentHtml + ahtml;
			//name
			NodeList titlelist = baseMing.getTag(childHtml, 5);
			TitleTag title = (TitleTag) titlelist.elementAt(0);
			dp.setName(title.getChildrenHTML());
			//price
			NodeList stronglist = baseMing.getTag(childHtml, 7);
			//			StrongTag strong = (StrongTag) stronglist.elementAt(0);
			//		    dp.setComment(strong.getChildrenHTML());
			for (int i = 0; i < stronglist.size(); i++) {
				StrongTag strong = (StrongTag) stronglist.elementAt(i);
				if (strong.getAttribute("class") != null && strong.getAttribute("class").equalsIgnoreCase("line"))
				{
					//		    		NodeList  sList  =  baseMing.getTag(strong.getChildrenHTML(), 7);//ParagraphTag
					//		    		StrongTag price = (StrongTag) sList.elementAt(0);
					//		    		System.out.println(pp.getChildrenHTML());
					dp.setComment(strong.getChildrenHTML());
					break;
				}
			}
			//现在做div
			NodeList divlist = baseMing.getTag(childHtml, 2);
			for (int i = 0; i < divlist.size(); i++)
			{
				//		    	System.out.println("1");
				Div div = (Div) divlist.elementAt(i);
				//		    	System.out.println("2");
				//		    	NodeList  pList  =  baseMing.getTag(div.getChildrenHTML(), 4);
				//		    	System.out.println("3");
				//		    	NodeList  imgList  =  baseMing.getTag(div.getChildrenHTML(), 8);
				//		    	System.out.println("4");
				//Description
				if (div.getAttribute("id") != null && div.getAttribute("id").equalsIgnoreCase("details"))
				{
					NodeList pList = baseMing.getTag(div.getChildrenHTML(), 4);
					ParagraphTag pp = (ParagraphTag) pList.elementAt(0);//ParagraphTag
					//		    		System.out.println(pp.getChildrenHTML());
					dp.setDescription(pp.getChildrenHTML());
				}
				//address  phone
				if (div.getAttribute("id") != null && div.getAttribute("id").equalsIgnoreCase("xq_thisr_temp"))
				{
					NodeList pList = baseMing.getTag(div.getChildrenHTML(), 4);
					ParagraphTag pp = (ParagraphTag) pList.elementAt(0);//ParagraphTag
					//		    		System.out.println(pp.getChildrenHTML());
					String temp = pp.getChildrenHTML();
					if ((temp.indexOf("地址：") >= 0)
							&& (temp.indexOf("<br />电话") >= 0) && (temp.indexOf("电话：") >= 0)
							&& (temp.indexOf("<br />交通") >= 0)) {
						String address = temp.substring(temp.indexOf("地址：") + 3, temp.indexOf("<br />电话"));
						String phone = temp.substring(temp.indexOf("电话：") + 3, temp.indexOf("；<br />交通"));
						dp.setAddress(address);
						dp.setPhone(phone);
					}
					//		    		dp.setDescription(pp.getChildrenHTML());
				}
				//图片
				if (div.getAttribute("id") != null && div.getAttribute("id").equalsIgnoreCase("imgBox"))
				{
					System.out.println("imgTag进来了");
					NodeList imgList = baseMing.getTag(div.getChildrenHTML(), 8);
					System.out.println(div.getTagBegin());
					//			    		  Div div1 = (Div)pList.elementAt(0);
					ImageTag it = (ImageTag) imgList.elementAt(0);//ParagraphTag
					it.getAttribute("src");//这就是图片的地址
					System.out.println(it.getAttribute("src"));

					URL url = new URL(it.getAttribute("src"));

					Date date1 = new Date();
					SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");
					String str1 = sdf1.format(date1);
					File outFile = new File("d:/work/hzf/map/" + str1 + ".jpg");
					OutputStream os = new FileOutputStream(outFile);
					InputStream is = url.openStream();
					byte[] buff = new byte[1024];
					while (true) {
						int readed = is.read(buff);
						if (readed == -1) {
							break;
						}
						byte[] temp = new byte[readed];
						System.arraycopy(buff, 0, temp, 0, readed);
						os.write(temp);
					}
					is.close();
					os.close();

				}
			}
			dplist.add(dp);
		}

		return dplist;
	}

	//在子网站list中取得对应的字段中的数据
	public List<String> catchData4(List<String> listHtml) throws Exception {
		//type :  0
		//description: <div class="bd"><!--详情标题--><p class="title">
		//name: <title>389元 昆大丽6日游,团购 -【窝窝网购】</title>
		//address: <div  id="xq_thisr_temp"
		//phone:   <div  id="xq_thisr_temp"
		//comment:这个网站没有评论
		/*
		 * <li class="price"> <span>原价<br><strong
		 * class="line">&yen;1480</strong></span>
		 * <span>折扣<br><strong>2.6折</strong></span>
		 * <span>节省<br><strong>&yen;1091</strong></span> </li>
		 */
		return listHtml;
	}

	//放入sqllite数据库中
	public boolean putList() {

		return false;
	}

	public static void main(String args[]) throws Exception {
		Wowo ww = new Wowo();
		String html = "http://lvyou.55tuan.com/kunming-1-0-0-1";
		String tag = "class";
		//		String keyword ="dh_spbox";//con_box clearfix
		String keyword = "dh_spbox";//con_box clearfix
		List<String> listTemp = ww.catchData1(html, tag, keyword);
		if (listTemp.size() > 0) {
			//			System.out.println(listTemp.get(0));
			List<dataPo> list23 = ww.catchData3(ww.catchData2(listTemp));
			for (dataPo dp : list23) {
				System.out.println(dp.getAddress());
			}
		} else {
			System.out.println("nothing");
		}

	}

}
