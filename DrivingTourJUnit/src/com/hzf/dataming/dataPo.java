package com.hzf.dataming;

import com.hzf.drivingtour.entity.AEntityBase;
 
public class dataPo  extends AEntityBase{

	private String type;//例如0为窝窝团
	private String map;//图片所在的路径地址，例：d:\work\ad.png
	private String description;
	private String name;
	private String address;
	private String phone;
	private String comment;
//	Log.e(TAG, e.getMessage(), e);
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public Object val(int index) {
		switch (index) {
		case 0:
			return get_ID();
		case 1:
			return type;
		case 2:
			return map;
		case 3:
			return description;
		case 4:
			return name;
		case 5:
			return address;
		case 6:
			return phone;
		case 7:
			return comment;
		default:
			return null;
		}
	}
	@Override
	public void val(int index, Object v) {
		switch (index) {
		case 0:
			set_ID((Integer) v);
			break;
		case 1:
			type = (String) v;
			break;			
		case 2:
			map = (String) v;
			break;
		case 3:
			description = (String) v;
			break;
		case 4:
			name = (String) v;
			break;
		case 5:
			address = (String) v;
			break;
		case 6:
			phone = (String) v;
			break;
		case 7:
			comment = (String) v;
			break;
		}
		
	}
	
}

