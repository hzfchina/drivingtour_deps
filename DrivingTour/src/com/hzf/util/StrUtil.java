package com.hzf.util;

public class StrUtil {
	public static String removePostfix(String str, String postfix) {
		String out = str.trim();
		if (out.endsWith(postfix)) {
			out = out.substring(0, out.length() - postfix.length());
		}
		return out;
	}

}
