package com.hzf.drivingtour;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.hzf.drivingtour.data.DataService;
import com.hzf.drivingtour.entity.ELocation;

public class DrivingTourApp extends Application {
	public static final String TAG = "com.hzf.drivingtour.DrivingTourApp";
	private static DrivingTourApp app = null;
	public static DataService service = null;
	public static ELocation myLocation = new ELocation(ELocation.Type_MyLocation, "我的位置", "", "", 25067217, 102669277);

	@Override
	public void onCreate() {
		super.onCreate();
		service = new DataService(getApplicationContext());
		app = this;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	public static void showToast(String msg) {
		Log.d(TAG, "[showToast]" + msg);
		Toast.makeText(app.getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	public static void showToast(int resId) {
		Log.d(TAG, "[showToast]" + app.getString(resId));
		Toast.makeText(app.getApplicationContext(), app.getString(resId), Toast.LENGTH_LONG).show();
	}
}
