package com.hzf.drivingtour.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.baidu.mapapi.GeoPoint;
import com.hzf.drivingtour.DrivingTourApp;
import com.hzf.drivingtour.R;
import com.hzf.drivingtour.entity.ELocation;
import com.hzf.drivingtour.map.MapUtil;
import com.hzf.drivingtour.map.SherlockMapActivity;

public class MapViewActivity extends SherlockMapActivity {
	private static final String TAG = MapViewActivity.class.getName();

	private Dialog dlgLayers = null;
	private int range = 50000;
	private boolean showMyLocation = true;
	private boolean[] showLocationTypes = new boolean[ItemizedOverlayTypes.length];
	private GeoPoint lastCenter = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//action bar
		ActionBar ab = getSupportActionBar();
		Context context = ab.getThemedContext();
		ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(context, R.array.MapViewActivityActions, R.layout.sherlock_spinner_item);
		list.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		//		ab.setListNavigationCallbacks(list, this);
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowTitleEnabled(false);
	}

	public void showSelLayerDlg() {
		if (dlgLayers == null) {
			String[] labels = new String[ItemizedOverlayLabels.length];
			for (int i = 0; i < labels.length; i++) {
				labels[i] = getString(ItemizedOverlayLabels[i]);
			}
			dlgLayers = new AlertDialog.Builder(this).setTitle("选择要显示的图层")
					.setMultiChoiceItems(labels, showLocationTypes, new OnMultiChoiceClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which, boolean isChecked) {
							showLocationTypes[which] = isChecked;
						}
					})
					.setNegativeButton("确定", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							showAllLocationOverlay();
						}
					}).setPositiveButton("取消", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dlgLayers.dismiss();
						}
					}).create();
		}
		dlgLayers.show();
	}

	public void showAllLocationOverlay() {
		List<Integer> types = new ArrayList<Integer>();
		int i = 0;
		for (boolean showLayer : showLocationTypes) {
			if (showLayer) {
				types.add(i);
			}
			i++;
		}
		List<ELocation> locations = DrivingTourApp.service.searchNearLocations(getMapCenter(), range, types.toArray(new Integer[] {}));
		clearAllOverlay();
		if (locations != null) {
			for (ELocation l : locations) {
				if (l != null) {
					addLocation(l);
				}
			}
			mapView.invalidate();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.mapview_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		GeoPoint gp;
		if (getString(R.string.app_name).trim().equalsIgnoreCase(item.getTitle().toString().trim())) {
			//back to home
			back2MainActivity();
		} else if (item.getItemId() == R.id.menu_layers) {
			showSelLayerDlg();
		} else if (item.getItemId() == R.id.menu_action1) {//显示所有图层
			showAllLocationOverlay();
		} else if (item.getItemId() == R.id.menu_action2) {//清除所有图层
			clearAllOverlay();
		} else if (item.getItemId() == R.id.menu_action3) {//我的位置
			showMyLocation = !showMyLocation;
			showMyLocationOverlay();
		} else if (item.getItemId() == R.id.menu_action4) {//下载离线地图
			downOfflineMap(104);//昆明
		} else if (item.getItemId() == R.id.menu_action5) {//显示中心坐标
			gp = mapView.getMapCenter();
			showLocation(gp);
			moveTo(gp);
			addOverlay(gp, 1, "test1", "test!!!!!!!!!!!");
			mapView.invalidate();
		} else {
			Log.d(TAG, "onOptionsItemSelected:" + Integer.toHexString(item.getItemId()) + "--" + item.getTitle());
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	public void onMapMoveFinish() {
		if (lastCenter == null) {
			lastCenter = DrivingTourApp.myLocation.getGeoPoint();
		}
		GeoPoint gp = getMapCenter();
		Log.d(TAG, MapUtil.inRange(gp, lastCenter, 10) + "--" + (
				Math.abs(gp.getLatitudeE6() - lastCenter.getLatitudeE6())
				) + "--" + (
				Math.abs(gp.getLongitudeE6() - lastCenter.getLongitudeE6())
				));
		if (lastCenter != null) {
			if (MapUtil.inRange(gp, lastCenter, 10)) {
				showAllLocationOverlay();
				showMyLocationOverlay();
			}
		}
		lastCenter = gp;
	}

	public void addLocation(ELocation location) {
		addOverlay(location.getGeoPoint(), location.getType(),
				location.getTitle(), location.getSnippet());
	}

	public void showMyLocationOverlay() {
		try {
			toogleMyLocation(showMyLocation);
			showLocation(DrivingTourApp.myLocation.getGeoPoint());
			//			if (showMyLocation) {
			//				Location lastLocation = DrivingTourApp.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			//				if (lastLocation != null) {
			//					showOverlays(showMyLocation, null)
			//					DrivingTourApp.myLocation.setPointX((int) (lastLocation.getLatitude() * 10e6));
			//					DrivingTourApp.myLocation.setPointY((int) (lastLocation.getLongitude() * 10e6));
			//					addLocation(DrivingTourApp.myLocation);
			//					showLocation(DrivingTourApp.myLocation.getGeoPoint());
			//				} else {
			//					Toast.makeText(this, "无法定位当前位置,请开启GPS重试!", Toast.LENGTH_LONG).show();
			//					DrivingTourApp.myLocation.setPointX(102669277);
			//					DrivingTourApp.myLocation.setPointY(25067217);
			//				}
			//				moveTo(DrivingTourApp.myLocation.getGeoPoint());
			//			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
	}

	private void showLocation(GeoPoint gp) {
		if (gp != null) {
			Log.d(TAG, "showLocation:" + gp.toString());
			Toast.makeText(this, gp.toString(), Toast.LENGTH_LONG).show();
		} else {
			Log.d(TAG, "showLocation: null");
			Toast.makeText(this, "showLocation: null", Toast.LENGTH_LONG).show();
		}
	}

	public void back2MainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
}
