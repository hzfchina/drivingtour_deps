package com.hzf.drivingtour.activity;

import com.hzf.drivingtour.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class GalleryAdapter extends BaseAdapter{
	
	private LayoutInflater mInflater;
	private int[] images;
   
	public GalleryAdapter(Context context,int[] images) {
		this.mInflater = LayoutInflater.from(context);
		this.images = images;
	}
	@Override
	public int getCount() {
		return images.length;
	}

	@Override
	public Object getItem(int position) {
		return images[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.gallery_adapter, null);
		ImageView image = (ImageView)convertView.findViewById(R.gallery.image);
		image.setBackgroundResource(images[position]);
		return convertView;
	}

}
