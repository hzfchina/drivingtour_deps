package com.hzf.drivingtour.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.hzf.drivingtour.R;
import com.hzf.drivingtour.entity.ERoute;

/**
 * @author wei.yang 2012-9-13
 * 详细信息描述页面
 */
public class DetailsActivity extends Activity{
	
	private TextView title,description,addressBtn,telBtn;
	private Button back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.details);
		
		Intent intent = getIntent();
		ERoute eroute = (ERoute)intent.getSerializableExtra("model");
		
		title = (TextView)this.findViewById(R.details.title);
		description = (TextView)this.findViewById(R.details.description);
		addressBtn = (TextView)this.findViewById(R.details.addressBtn);
		telBtn = (TextView)this.findViewById(R.details.telBtn);
		back = (Button)this.findViewById(R.details.back);
		
		if(eroute != null){
			title.setText(eroute.getTitle());
			description.setText(eroute.getDescription());
		}
		
		listener(back);
		listener(addressBtn);
		listener(telBtn);
	}
	
	private void listener(View view){
		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.details.back:
					finish();
					break;
				case R.details.addressBtn:
					Intent intent = new Intent(DetailsActivity.this,MapViewActivity.class);
					startActivity(intent);
					break;
				case R.details.telBtn:
					String mobile = "0871-3524063";
					Intent intentPhone = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
					startActivity(intentPhone);
					break;
				default:
					break;
				}
			}
		});
	}
}
