package com.hzf.drivingtour.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost;

import com.hzf.drivingtour.R;

/**
 * @author wei.yang 2012-9-10 tabHostҳ��
 */
@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {

	private RadioGroup group;
	private TabHost tabHost;
	private Button btn_search, btn_map;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		tabHost = getTabHost();
		group = (RadioGroup) findViewById(R.main.main_radio);
		btn_search = (Button) findViewById(R.main.btn_search);
		btn_map = (Button) findViewById(R.main.btn_map);

		tabHost.addTab(tabHost.newTabSpec("线路推荐")
				.setIndicator("线路推荐")
				.setContent(new Intent(this, LineRecommendActivity.class)));
		tabHost.addTab(tabHost.newTabSpec("景点")
				.setIndicator("景点")
				.setContent(new Intent(this, SceneryActivity.class)));
		tabHost.addTab(tabHost.newTabSpec("酒店")
				.setIndicator("酒店")
				.setContent(new Intent(this, HotelActivity.class)));
		tabHost.addTab(tabHost.newTabSpec("饭点")
				.setIndicator("饭点")
				.setContent(new Intent(this, RiceActivity.class)));
		//设置线路推荐未开始界面
		((RadioButton) findViewById(R.main.line_recommend)).setChecked(true);
		//RadioGroup的点击事件
		group.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.main.line_recommend:
					tabHost.setCurrentTab(0);
					break;
				case R.main.scenery:
					tabHost.setCurrentTab(1);
					break;
				case R.main.hotel:
					tabHost.setCurrentTab(2);
					break;
				case R.main.rice:
					tabHost.setCurrentTab(3);
					break;
				default:
					break;
				}
			}
		});

		listener(btn_search);
		listener(btn_map);
	}

	/**
	 * @param view
	 *            控件的点击事件
	 */
	private void listener(View view) {
		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				switch (v.getId()) {
				case R.main.btn_search:
					intent.setClass(MainActivity.this, SearchActivity.class);
					startActivity(intent);
					break;
				case R.main.btn_map:
					intent.setClass(MainActivity.this, MapViewActivity.class);
					startActivity(intent);
					break;
				default:
					break;
				}
			}
		});
	}
}