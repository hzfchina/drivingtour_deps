package com.hzf.drivingtour.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.hzf.drivingtour.DrivingTourApp;
import com.hzf.drivingtour.R;
import com.hzf.drivingtour.entity.ERoute;

public class SearchResultActivity extends Activity {

	private AutoCompleteTextView autoCompleteTextView;
	private List<ERoute> list;
	private List<String> listSearch = new ArrayList<String>();
	private Button btn_search;
	private String text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_result);

		list = DrivingTourApp.service.getList();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				listSearch.add(list.get(i).getTitle());
			}
		}

		autoCompleteTextView = (AutoCompleteTextView)
				findViewById(R.search_result.autoCompleteTextView);
		btn_search = (Button) findViewById(R.search_result.btn_search);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchResultActivity.this,
				android.R.layout.simple_dropdown_item_1line, listSearch);
		autoCompleteTextView.setAdapter(adapter);

		listener(btn_search);
	}

	private void listener(View view) {
		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.search_result.btn_search:
					if (validate()) {
						Intent intent = new Intent(SearchResultActivity.this, MainActivity.class);
						intent.putExtra("condition", text);
						startActivity(intent);
						finish();
					} else {
						Toast.makeText(SearchResultActivity.this, "请输入搜索条件", Toast.LENGTH_SHORT).show();
					}
					break;
				default:
					break;
				}
			}
		});
	}

	private boolean validate() {
		boolean b = false;
		text = autoCompleteTextView.getText().toString().trim();
		if (text != null && !"".equals(text)) {
			b = true;
		}
		return b;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(SearchResultActivity.this, MainActivity.class);
			startActivity(intent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
