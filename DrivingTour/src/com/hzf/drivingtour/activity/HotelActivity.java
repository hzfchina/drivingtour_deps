package com.hzf.drivingtour.activity;

import java.util.List;

import com.hzf.drivingtour.DrivingTourApp;
import com.hzf.drivingtour.R;
import com.hzf.drivingtour.entity.ERoute;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;


/**
 * @author wei.yang 2012-9-13
 * 酒店页面
 */
public class HotelActivity extends Activity{
	
	private ListView listView;
	private List<ERoute> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scenery);
		
		listView = (ListView)this.findViewById(R.id.listView);
		list = DrivingTourApp.service.getList();
		
		if(list != null && list.size()>0){
			listView.setAdapter(new ListAdapter(this, list));
		}
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ERoute eroute = list.get(arg2);
				Intent intent = new Intent(HotelActivity.this,DetailsActivity.class);
				intent.putExtra("model", eroute);
				startActivity(intent);
			}
		});
	}
}
