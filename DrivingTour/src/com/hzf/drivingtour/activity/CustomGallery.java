package com.hzf.drivingtour.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;


/**
 * @author wei.yang 2012-9-13
 * 自定义Gallery防止滑动过程中半屏滚动 
 */
public class CustomGallery extends Gallery {

	public CustomGallery(Context context) {
		super(context);
	}

	public CustomGallery(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CustomGallery(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

}
