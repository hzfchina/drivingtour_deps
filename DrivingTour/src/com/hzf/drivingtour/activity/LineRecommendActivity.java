package com.hzf.drivingtour.activity;

import com.hzf.drivingtour.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.AdapterView.OnItemSelectedListener;


/**
 * @author wei.yang 2012-9-13
 * 线路推荐
 */
public class LineRecommendActivity extends Activity{
	
	private CustomGallery gallery;
	private RadioGroup point;
	private int[] images = {R.drawable.line0,R.drawable.line1,
		R.drawable.line2};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.line_recommend);
		
		gallery = (CustomGallery)this.findViewById(R.line.gallery);
		point = (RadioGroup)this.findViewById(R.line.point);
		for(int i=0;i<images.length;i++){
			RadioButton rb = new RadioButton(this);
			rb.setLayoutParams(new LayoutParams(15, 15));
			final int temp = i;
			rb.setId(temp);
			rb.setButtonDrawable(R.drawable.point_gray);
			point.addView(rb);
		}
		gallery.setAdapter(new GalleryAdapter(this, images));
		
		//gallery滑动事件
		gallery.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				for (int i = 0; i < images.length; i++) {
					RadioButton button = (RadioButton) findViewById(i);
					button.setButtonDrawable(R.drawable.point_gray);
				}
				RadioButton rb = (RadioButton) findViewById(position);
				rb.setButtonDrawable(R.drawable.point_white);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
	}
}
