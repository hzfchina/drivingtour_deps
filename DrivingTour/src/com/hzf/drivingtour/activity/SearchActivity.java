package com.hzf.drivingtour.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.hzf.drivingtour.DrivingTourApp;
import com.hzf.drivingtour.R;
import com.hzf.drivingtour.entity.ERoute;

/**
 * @author wei.yang 2012-9-13
 * 搜索界面
 */
public class SearchActivity extends Activity{
	
	private AutoCompleteTextView autoCompleteTextView;
	private List<ERoute> list,listSearchResult;
	private List<String> listSearch = new ArrayList<String>();
	private Button btn_search;
	private String text;
	private ProgressDialog progress;
	private ListView listView;
	private TextView content;
 	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(msg.what == 1){
				progress.dismiss();
				if(listSearchResult != null && listSearchResult.size() > 0){
					listView.setVisibility(View.VISIBLE);
					content.setVisibility(View.GONE);
					listView.setAdapter(new ListAdapter(SearchActivity.this, listSearchResult));
				}else{
					listView.setVisibility(View.GONE);
					content.setVisibility(View.VISIBLE);
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		
		list = DrivingTourApp.service.getList();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				listSearch.add(list.get(i).getTitle());
			}
		}
		progress = new ProgressDialog(this);
		progress.setMessage("正在获取数据,请稍后...");

		listView = (ListView)this.findViewById(R.search.listView);
		content = (TextView)this.findViewById(R.search.content);
		autoCompleteTextView = (AutoCompleteTextView)
				findViewById(R.search.autoCompleteTextView);
		btn_search = (Button) findViewById(R.search.btn_search);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchActivity.this,
				android.R.layout.simple_dropdown_item_1line, listSearch);
		autoCompleteTextView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ERoute eroute = listSearchResult.get(arg2);
				Intent intent = new Intent(SearchActivity.this,DetailsActivity.class);
				intent.putExtra("model", eroute);
				startActivity(intent);
			}
		});

		listener(btn_search);
	}
	
	private void listener(View view) {
		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.search.btn_search:
					if (validate()) {
						progress.show();
						new Thread(new Runnable() {
							@Override
							public void run() {
								listSearchResult = DrivingTourApp.service.getConditionList(text);
								handler.sendEmptyMessage(1);
							}
						}).start();
					} else {
						Toast.makeText(SearchActivity.this, "请输入搜索条件", Toast.LENGTH_SHORT).show();
					}
					break;
				default:
					break;
				}
			}
		});
	}

	private boolean validate() {
		boolean b = false;
		text = autoCompleteTextView.getText().toString().trim();
		if (text != null && !"".equals(text)) {
			b = true;
		}
		return b;
	}

}
