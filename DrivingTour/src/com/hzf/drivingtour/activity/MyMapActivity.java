package com.hzf.drivingtour.activity;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.LocationListener;
import com.baidu.mapapi.MKLocationManager;
import com.baidu.mapapi.MKMapViewListener;
import com.baidu.mapapi.MKOLUpdateElement;
import com.baidu.mapapi.MKOfflineMap;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MyLocationOverlay;
import com.baidu.mapapi.Overlay;
import com.baidu.mapapi.OverlayItem;
import com.hzf.drivingtour.DrivingTourApp;
import com.hzf.drivingtour.R;
import com.hzf.drivingtour.map.BaiduLocationListener;
import com.hzf.drivingtour.map.MapKeyGeneralListener;
import com.hzf.drivingtour.map.MyMKOfflineMapListener;
import com.hzf.drivingtour.map.OnSingleTapListener;
import com.hzf.drivingtour.map.SimpleItemizedOverlay;
import com.hzf.drivingtour.map.TapControlledMapView;

public abstract class MyMapActivity extends MapActivity implements MKMapViewListener {
	//	private static final String TAG = MyMapActivity.class.getName();
	private static final String mapApiKey = "589BFEBBE2880C4F6F8F24891717E8C449CECDF6";
	public static final int[] ItemizedOverlayTypes = new int[] {
			R.drawable.class_dianying,
			R.drawable.class_jiudian,
			R.drawable.class_lvyou,
			R.drawable.class_meirong,
			R.drawable.class_meishi,
			R.drawable.class_shenghuo,
			R.drawable.class_yule
			//			R.drawable.ic_category_hotel,
			//			R.drawable.ic_category_restrant,
			//			R.drawable.ic_category_hotspots,
			//			R.drawable.ic_category_gasstation,
			//			R.drawable.ic_category_mall,
			//			R.drawable.ic_category_atm,
			//			R.drawable.ic_category_bar,
			//			R.drawable.ic_category_bath,
			//			R.drawable.ic_category_busstop,
			//			R.drawable.ic_category_camera,
			//			R.drawable.ic_category_gym,
			//			R.drawable.ic_category_hospital,
			//			R.drawable.ic_category_ktv,
			//			R.drawable.ic_category_medical,
			//			R.drawable.ic_category_parking,
			//			R.drawable.ic_category_stadium,
			//			R.drawable.ic_category_supermarket,
			//			R.drawable.ic_category_bank
	};
	public static final int[] ItemizedOverlayLabels = new int[] {
			R.string.Type_Hotle,
			R.string.Type_Restrant,
			R.string.Type_HotSpots,
			R.string.Type_GasStation,
			R.string.Type_Mall,
			R.string.Type_ATM,
			R.string.Type_Bar
			//			R.string.Type_Bath,
			//			R.string.Type_BusStop,
			//			R.string.Type_Camera,
			//			R.string.Type_Gym,
			//			R.string.Type_Hospital,
			//			R.string.Type_KTV,
			//			R.string.Type_Medical,
			//			R.string.Type_Parking,
			//			R.string.Type_Stadium,
			//			R.string.Type_SuperMarket,
			//			R.string.Type_Bank
	};

	protected BMapManager bMapManager = null;
	protected MKLocationManager mLocationManager = null;
	protected LocationListener mLocationListener = null;
	protected TapControlledMapView mapView;
	protected MyLocationOverlay me = null;
	private SimpleItemizedOverlay[] itemizedOverlays = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Sherlock_Light);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_view);

		//bMapManager
		bMapManager = new BMapManager(this);
		bMapManager.init(mapApiKey, new MapKeyGeneralListener());
		bMapManager.getLocationManager().setNotifyInternal(10, 5);
		bMapManager.start();
		initMapActivity(bMapManager);

		//mapView
		mapView = (TapControlledMapView) findViewById(R.id.bmapView);
		mapView.setBuiltInZoomControls(true);
		mapView.setTraffic(false);
		mapView.setDoubleClickZooming(false);
		mapView.setOnSingleTapListener(new OnSingleTapListener() {
			@Override
			public boolean onSingleTap(MotionEvent e) {
				if (itemizedOverlays != null) {
					for (SimpleItemizedOverlay itemizedOverlay : itemizedOverlays) {
						if (itemizedOverlay != null) {
							itemizedOverlay.hideAllBalloons();
						}
					}
				}
				return true;
			}
		});
		mapView.regMapViewListener(bMapManager, this);

		me = new MyLocationOverlay(this, mapView);
		itemizedOverlays = new SimpleItemizedOverlay[ItemizedOverlayTypes.length];

		//
		mLocationListener = new BaiduLocationListener();
		//获得定位服务
		mLocationManager = bMapManager.getLocationManager();
		//设置监听间隔
		mLocationManager.setNotifyInternal(10, 5);
		// 使用GPS定位
		mLocationManager.enableProvider(MKLocationManager.MK_GPS_PROVIDER);

		//MapController
		final MapController mc = mapView.getController();
		mc.setZoom(16);
		mc.setCenter(DrivingTourApp.myLocation.getGeoPoint());
		//		mapView.setStreetView(false);
	}

	public void toogleMyLocation(boolean show) {
		if (show) {
			me.enableCompass();
			me.enableMyLocation();
			//注册监听事件
			mLocationManager.requestLocationUpdates(mLocationListener);
			showOverlays(true, me);
			moveTo(DrivingTourApp.myLocation.getGeoPoint());
			mapView.invalidate();
		} else {
			me.disableCompass();
			me.disableMyLocation();
			showOverlays(false, me);
			//不需要定位时移除监听事件
			mLocationManager.removeUpdates(mLocationListener);
		}
	}

	public void addOverlay(GeoPoint point, int type, String title, String snippet) {
		OverlayItem overlayItem = new OverlayItem(point, title, snippet);
		SimpleItemizedOverlay itemizedOverlay = itemizedOverlays[type];
		if (itemizedOverlay == null) {
			Drawable drawable = getResources().getDrawable(ItemizedOverlayTypes[type]);
			itemizedOverlay = new SimpleItemizedOverlay(drawable, mapView);
			itemizedOverlay.setShowClose(false);
			itemizedOverlay.setShowDisclosure(false);
			itemizedOverlay.setSnapToCenter(false);
			itemizedOverlays[type] = itemizedOverlay;
		}
		itemizedOverlay.addOverlay(overlayItem);
		showOverlays(true, itemizedOverlay);
	}

	public void showOverlays(boolean isShow, Overlay o) {
		List<Overlay> mapOverlays = mapView.getOverlays();
		if (!mapOverlays.contains(o)) {
			if (isShow) {
				mapOverlays.add(o);
			} else {
				mapOverlays.remove(o);
			}
		}
	}

	@Override
	protected void onPause() {
		toogleMyLocation(false);
		bMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		toogleMyLocation(true);
		bMapManager.start();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		if (bMapManager != null) {
			bMapManager.destroy();
			bMapManager = null;
		}
		super.onDestroy();
	}

	@Override
	protected boolean isLocationDisplayed() {
		return me.isMyLocationEnabled();
	}

	public GeoPoint getMapCenter() {
		return mapView.getMapCenter();
	}

	public void clearAllOverlay() {
		mapView.getOverlays().clear();
		mapView.invalidate();
	}

	public void downOfflineMap(int cityID) {
		MKOfflineMap mOffline = new MKOfflineMap();
		mOffline.init(bMapManager, new MyMKOfflineMapListener(mOffline));
		ArrayList<MKOLUpdateElement> info = mOffline.getAllUpdateInfo();
		if (info != null) {
			for (MKOLUpdateElement updateElement : info) {
				if (updateElement.cityID == cityID) {
					if (updateElement.status == MKOLUpdateElement.FINISHED) {
						return;
					}
				}
			}
		}
		mOffline.start(cityID);
	}

	public void moveTo(GeoPoint point) {
		mapView.getController().setCenter(point);
		mapView.getController().animateTo(point);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

}
