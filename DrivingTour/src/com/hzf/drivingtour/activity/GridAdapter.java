package com.hzf.drivingtour.activity;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hzf.drivingtour.R;

public class GridAdapter extends BaseAdapter{

	private int image[] = {R.drawable.icon_zhengcan,R.drawable.icon_kuaican,
		R.drawable.icon_xiaochi,R.drawable.icon_yingpin,R.drawable.icon_xican};
	private int image_bk[] = {R.drawable.icon_zhengcan_bk,R.drawable.icon_kuaican_bk,
			R.drawable.icon_xiaochi_bk,R.drawable.icon_yingpin_bk,R.drawable.icon_xican_bk};
	private String str[] = {"酒店","饭点","景点","加油站","便利店"};
	private LayoutInflater inflater;
	private int biaoji;

	public void setBiaoji(int biaoji) {
		this.biaoji = biaoji;
	}

	public GridAdapter(Context context){
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return image.length;
	}

	@Override
	public Object getItem(int position) {
		return image[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = inflater.inflate(R.layout.grid_item, null);
		ImageView itemImage = (ImageView)view.findViewById(R.id.itemImage);
		TextView itemText = (TextView)view.findViewById(R.id.itemText);
		itemImage.setBackgroundResource(image[position]);
		itemText.setText(str[position]);
		if(biaoji == position){
			itemText.setTextColor(Color.RED);
			itemImage.setBackgroundResource(image_bk[position]);
		}else{
			itemText.setTextColor(Color.BLACK);
		}
		return view;
	}

}
