package com.hzf.drivingtour.activity;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hzf.drivingtour.R;
import com.hzf.drivingtour.entity.ERoute;

public class ListAdapter extends BaseAdapter{

	private List<ERoute> list;
	private LayoutInflater inflater;

	public ListAdapter(Context context,List<ERoute> list){
		inflater = LayoutInflater.from(context);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = inflater.inflate(R.layout.list_item, null);
		ERoute eroute = list.get(position);
		TextView title = (TextView)view.findViewById(R.item.title);
		title.setText(eroute.getTitle());
		return view;
	}

}
