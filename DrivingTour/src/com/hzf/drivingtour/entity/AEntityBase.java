package com.hzf.drivingtour.entity;

import android.database.Cursor;
import android.util.Log;

public abstract class AEntityBase {
	private final static String TAG = AEntityBase.class.getName();
	private int _ID = -1;

	public int get_ID() {
		return _ID;
	}

	public void set_ID(int _ID) {
		this._ID = _ID;
	}

	public abstract Object val(int index);

	public abstract void val(int index, Object v);

	public AEntityBase build(Cursor c, String prefix) {
		String[] Columns = getColumns();
		int[] Column_Types = getColumn_Types();
		for (int i = 0; i < Columns.length; i++) {
			int index = c.getColumnIndex(prefix + Columns[i]);
			if (Column_Types[i] == 2) {
				this.val(i, c.getInt(index));
			} else if (Column_Types[i] == 1) {
				this.val(i, c.getString(index));
			}
		}
		return this;
	}

	public String getTable_Name() {
		if (this instanceof ERoute) {
			return ERoute.Table_Name;
		} else if (this instanceof ELocation) {
			return ELocation.Table_Name;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public String[] getColumns() {
		if (this instanceof ERoute) {
			return ERoute.Columns;
		} else if (this instanceof ELocation) {
			return ELocation.Columns;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public int[] getColumn_Types() {
		if (this instanceof ERoute) {
			return ERoute.Column_Types;
		} else if (this instanceof ELocation) {
			return ELocation.Column_Types;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void fillEntity(Cursor c) {
		try {
			int i = 0;
			for (int cloumntype : getColumn_Types()) {
				switch (cloumntype) {
				case 1:
					this.val(i, c.getString(i));
					break;
				case 2:
					this.val(i, c.getInt(i));
					break;
				case 3:
					this.val(i, c.getDouble(i));
					break;
				}
				i++;
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
	}
}
