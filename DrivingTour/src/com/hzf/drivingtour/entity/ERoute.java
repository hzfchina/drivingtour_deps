package com.hzf.drivingtour.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.baidu.mapapi.GeoPoint;

public class ERoute extends AEntityBase implements Serializable {
	public static final String TAG = ERoute.class.getName();
	private static final long serialVersionUID = 1L;
	public static final String Table_Name = "Route";
	public static final String[] Columns = new String[] {
			"_ID", "title", "snippet", "description", "startX", "startY", "endX", "endY", "points"
	};
	public static final int[] Column_Types = new int[] { 2, 1, 1, 1, 2, 2, 2, 2, 1 };
	private String title = "";
	private String snippet = "";
	private String description = "";
	private int startX;
	private int startY;
	private int endX;
	private int endY;
	private String points = "";

	public ERoute() {

	}

	public ERoute(String title, String snippet, String description, int startX,
			int startY, int endX, int endY, String points) {
		super();
		this.title = title;
		this.snippet = snippet;
		this.description = description;
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
		this.points = points;
	}

	public GeoPoint getStartGeoPoint() {
		return new GeoPoint(startX, startY);
	}

	public GeoPoint getEndGeoPoint() {
		return new GeoPoint(endX, endY);
	}

	public void setStartGeoPoint(GeoPoint p) {
		startX = p.getLatitudeE6();
		startY = p.getLongitudeE6();
	}

	public void setEndGeoPoint(GeoPoint p) {
		endX = p.getLatitudeE6();
		endY = p.getLongitudeE6();
	}

	public List<GeoPoint> parseGeoPoints() {
		List<GeoPoint> gps = new ArrayList<GeoPoint>();
		if (points != null) {
			String[] ps = points.split(",");
			if (ps != null && ps.length > 0 && ps.length % 2 == 0) {
				for (int i = 0; i < ps.length; i += 2) {
					try {
						int x = Integer.parseInt(ps[i]);
						int y = Integer.parseInt(ps[i + 1]);
						gps.add(new GeoPoint(x, y));
					} catch (Exception e) {
						Log.e(TAG, e.getMessage(), e);
					}
				}
			}
		}
		return gps;
	}

	@Override
	public Object val(int index) {
		switch (index) {
		case 0:
			return get_ID();
		case 1:
			return title;
		case 2:
			return snippet;
		case 3:
			return description;
		case 4:
			return startX;
		case 5:
			return startY;
		case 6:
			return endX;
		case 7:
			return endY;
		case 8:
			return points;
		default:
			return null;
		}
	}

	@Override
	public void val(int index, Object v) {
		switch (index) {
		case 0:
			set_ID((Integer) v);
			break;
		case 1:
			title = (String) v;
			break;
		case 2:
			snippet = (String) v;
			break;
		case 3:
			description = (String) v;
			break;
		case 4:
			startX = (Integer) v;
			break;
		case 5:
			endX = (Integer) v;
			break;
		case 6:
			endX = (Integer) v;
			break;
		case 7:
			endY = (Integer) v;
			break;
		case 8:
			points = (String) v;
			break;
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public int getEndX() {
		return endX;
	}

	public void setEndX(int endX) {
		this.endX = endX;
	}

	public int getEndY() {
		return endY;
	}

	public void setEndY(int endY) {
		this.endY = endY;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}
}
