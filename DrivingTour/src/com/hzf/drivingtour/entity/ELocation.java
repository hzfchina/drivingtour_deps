package com.hzf.drivingtour.entity;

import com.baidu.mapapi.GeoPoint;

public class ELocation extends AEntityBase {
	public static final int Type_UNKNOWN = 0;//未知
	public static final int Type_Start = 1;//起点
	public static final int Type_End = 2;//终点
	public static final int Type_MyLocation = 3;//我的位置
	public static final int Type_Hotle = 4;//酒店
	public static final int Type_Restrant = 5;//饭点
	public static final int Type_HotSpots = 6;//景点
	public static final int Type_GasStation = 7;//加油站
	public static final int Type_Mall = 8;//便利店/购物商场
	public static final int Type_ATM = 9;//ATM
	public static final int Type_Bar = 10;//酒吧
	public static final int Type_Bath = 11;//浴室
	public static final int Type_BusStop = 12;//公家车站
	public static final int Type_Camera = 13;//电影院
	public static final int Type_Gym = 14;//健身房
	public static final int Type_Hospital = 15;//医院
	public static final int Type_KTV = 16;//KTV
	public static final int Type_Medical = 17;//药店
	public static final int Type_Parking = 18;//公园
	public static final int Type_Stadium = 19;//体育馆
	public static final int Type_SuperMarket = 20;//超市
	public static final int Type_Bank = 21;//银行

	public static final String Table_Name = "Location";
	public static final String[] Columns = new String[] {
			"_ID", "type", "title", "snippet", "description", "pointX", "pointY"
	};
	public static final int[] Column_Types = new int[] { 2, 2, 1, 1, 1, 2, 2 };

	private int type = Type_UNKNOWN;
	private String title = "";
	private String snippet = "";
	private String description = "";
	private int pointX;
	private int pointY;

	public ELocation() {
		super();
	}

	public ELocation(int type, String title, String snippet, String description, int pointX, int pointY) {
		super();
		this.type = type;
		this.title = title;
		this.snippet = snippet;
		this.description = description;
		this.pointX = pointX;
		this.pointY = pointY;
	}

	public GeoPoint getGeoPoint() {
		return new GeoPoint(pointX, pointY);
	}

	public void setGeoPoint(GeoPoint p) {
		pointX = p.getLatitudeE6();
		pointY = p.getLongitudeE6();
	}

	@Override
	public Object val(int index) {
		switch (index) {
		case 0:
			return get_ID();
		case 1:
			return type;
		case 2:
			return title;
		case 3:
			return snippet;
		case 4:
			return description;
		case 5:
			return pointX;
		case 6:
			return pointY;
		default:
			return null;
		}
	}

	@Override
	public void val(int index, Object v) {
		switch (index) {
		case 0:
			set_ID((Integer) v);
			break;
		case 1:
			type = (Integer) v;
			break;
		case 2:
			title = (String) v;
			break;
		case 3:
			snippet = (String) v;
			break;
		case 4:
			description = (String) v;
			break;
		case 5:
			pointX = (Integer) v;
			break;
		case 6:
			pointY = (Integer) v;
			break;
		}
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPointX() {
		return pointX;
	}

	public void setPointX(int pointX) {
		this.pointX = pointX;
	}

	public int getPointY() {
		return pointY;
	}

	public void setPointY(int pointY) {
		this.pointY = pointY;
	}
}
