package com.hzf.drivingtour.map;

import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import com.baidu.mapapi.GeoPoint;
import com.hzf.drivingtour.entity.ERoute;
import com.hzf.util.StrUtil;

public class MapUtil {
	static double DEF_PI = 3.14159265359; // PI
	static double DEF_2PI = 6.28318530712; // 2*PI
	static double DEF_PI180 = 0.01745329252; // PI/180.0
	static double DEF_R = 6370693.5; // radius of earth

	public static double getShortDistance(double lon1, double lat1, double lon2, double lat2) {
		double ew1, ns1, ew2, ns2;
		double dx, dy, dew;
		double distance;
		// 角度转换为弧度
		ew1 = lon1 * DEF_PI180;
		ns1 = lat1 * DEF_PI180;
		ew2 = lon2 * DEF_PI180;
		ns2 = lat2 * DEF_PI180;
		// 经度差
		dew = ew1 - ew2;
		// 若跨东经和西经180 度，进行调整
		if (dew > DEF_PI) {
			dew = DEF_2PI - dew;
		} else if (dew < -DEF_PI) {
			dew = DEF_2PI + dew;
		}
		dx = DEF_R * Math.cos(ns1) * dew; // 东西方向长度(在纬度圈上的投影长度)
		dy = DEF_R * (ns1 - ns2); // 南北方向长度(在经度圈上的投影长度)
		// 勾股定理求斜边长
		distance = Math.sqrt(dx * dx + dy * dy);
		return distance;
	}

	public static double getLongDistance(double lon1, double lat1, double lon2, double lat2) {
		double ew1, ns1, ew2, ns2;
		double distance;
		// 角度转换为弧度
		ew1 = lon1 * DEF_PI180;
		ns1 = lat1 * DEF_PI180;
		ew2 = lon2 * DEF_PI180;
		ns2 = lat2 * DEF_PI180;
		// 求大圆劣弧与球心所夹的角(弧度)
		distance = Math.sin(ns1) * Math.sin(ns2) + Math.cos(ns1) * Math.cos(ns2) * Math.cos(ew1 - ew2);
		// 调整到[-1..1]范围内，避免溢出
		if (distance > 1.0) {
			distance = 1.0;
		} else if (distance < -1.0) {
			distance = -1.0;
		}
		// 求大圆劣弧长度
		distance = DEF_R * Math.acos(distance);
		return distance;
	}

	public static boolean inRange(GeoPoint src, GeoPoint target, int range) {
		if (src == null || target == null) {
			return false;
		}
		return Math.abs(src.getLatitudeE6() - target.getLatitudeE6()) > range
				|| Math.abs(src.getLongitudeE6() - target.getLongitudeE6()) > range;
	}

	public static double distance(GeoPoint s, GeoPoint t) {
		return getShortDistance(
				s.getLongitudeE6() / 10e6, s.getLatitudeE6() / 10e6,
				t.getLongitudeE6() / 10e6, t.getLatitudeE6() / 10e6);
	}

	public static ERoute parseKML(InputStream is) {
		ERoute vo = new ERoute();
		XmlPullParserFactory factory;
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(false);
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(is, null);
			int type = parser.getEventType();
			String path = "";
			int placemarkIndex = 0;
			while (type != XmlPullParser.END_DOCUMENT) {
				switch (type) {
				case XmlPullParser.START_DOCUMENT:
					break;
				case XmlPullParser.START_TAG:
					path += "/" + parser.getName();
					if ("Placemark".equalsIgnoreCase(parser.getName())) {
						placemarkIndex++;
					}
					break;
				case XmlPullParser.END_TAG:
					path = path.substring(0, path.lastIndexOf('/'));
					break;
				case XmlPullParser.TEXT:
					if ("/kml/Document/name".equalsIgnoreCase(path)) {
						vo.setTitle(parser.getText());
					} else if ("/kml/Document/description".equalsIgnoreCase(path)) {
						vo.setDescription(parser.getText());
					} else if (path.trim().endsWith("/coordinates")) {
						String text = parser.getText();
						if (placemarkIndex == 1) {
							vo.setStartGeoPoint(parseGeoPoint(text));
						} else if (placemarkIndex == 3) {
							vo.setEndGeoPoint(parseGeoPoint(text));
						} else {
							String[] points = text.split("\n");
							if (points != null) {
								for (String point : points) {
									GeoPoint gp = parseGeoPoint(point);
									vo.setPoints(vo.getPoints() + gp.getLatitudeE6() + "," + gp.getLongitudeE6() + ",");
								}
							}
						}
					}
					break;
				default:
					break;
				}
				parser.next();
				type = parser.getEventType();
			}
			vo.setPoints(StrUtil.removePostfix(vo.getPoints(), ","));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;
	}

	public static int parsePoint(String p) {
		return (int) (Double.parseDouble(p.trim()) * 1E6);
	}

	public static GeoPoint parseGeoPoint(String point) {
		String[] values = point.split(",");
		if (values != null && values.length > 2) {
			return new GeoPoint(parsePoint(values[1]), parsePoint(values[0]));
		}
		return null;
	}
}
