package com.hzf.drivingtour.map;

import android.util.Log;

import com.baidu.mapapi.MKOLUpdateElement;
import com.baidu.mapapi.MKOfflineMap;
import com.baidu.mapapi.MKOfflineMapListener;
import com.hzf.drivingtour.DrivingTourApp;

public class MyMKOfflineMapListener implements MKOfflineMapListener {
	public static final String TAG = MyMKOfflineMapListener.class.getName();
	private MKOfflineMap mOffline = null;

	public MyMKOfflineMapListener(MKOfflineMap mOffline) {
		this.mOffline = mOffline;
	}

	@Override
	public void onGetOfflineMapState(int type, int state) {
		switch (type) {
		case MKOfflineMap.TYPE_DOWNLOAD_UPDATE:
			Log.d(TAG, String.format("cityid:%d update", state));
			MKOLUpdateElement update = mOffline.getUpdateInfo(state);
			DrivingTourApp.showToast(String.format("%s : %d%%", update.cityName, update.ratio));
			break;
		case MKOfflineMap.TYPE_NEW_OFFLINE:
			Log.d(TAG, String.format("add offlinemap num:%d", state));
			break;
		case MKOfflineMap.TYPE_VER_UPDATE:
			Log.d(TAG, String.format("new offlinemap ver"));
			break;
		}
	}
}
