package com.hzf.drivingtour.map;

import android.location.Location;
import android.util.Log;

import com.baidu.mapapi.LocationListener;
import com.hzf.drivingtour.DrivingTourApp;

public class BaiduLocationListener implements LocationListener {
	private static final String TAG = BaiduLocationListener.class.getName();

	@Override
	public void onLocationChanged(Location location) {
		//当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
		Log.d(TAG, "onLocationChanged:" + location.getLatitude() + "--" + location.getLongitude());
		if (location != null) {
			DrivingTourApp.myLocation.setPointX((int) (location.getLatitude() * 10e6));
			DrivingTourApp.myLocation.setPointY((int) (location.getLongitude() * 10e6));
		}
	}
}
