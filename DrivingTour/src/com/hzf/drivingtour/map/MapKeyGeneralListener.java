package com.hzf.drivingtour.map;

import android.util.Log;

import com.baidu.mapapi.MKEvent;
import com.baidu.mapapi.MKGeneralListener;
import com.hzf.drivingtour.DrivingTourApp;

public class MapKeyGeneralListener implements MKGeneralListener {
	public static final String TAG = MapKeyGeneralListener.class.getName();

	@Override
	public void onGetNetworkState(int iError) {
		Log.d(TAG, "onGetNetworkState error is " + iError);
		DrivingTourApp.showToast("您的网络出错啦！");
	}

	@Override
	public void onGetPermissionState(int iError) {
		Log.d(TAG, "onGetPermissionState error is " + iError);
		if (iError == MKEvent.ERROR_PERMISSION_DENIED) {
			DrivingTourApp.showToast("地图API授权Key错误！");
		}
	}
}
