package com.hzf.drivingtour.map;

import android.view.MotionEvent;

public interface OnSingleTapListener {
	public boolean onSingleTap(MotionEvent e);
}
