package com.hzf.drivingtour.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.baidu.mapapi.GeoPoint;
import com.hzf.drivingtour.entity.ELocation;
import com.hzf.drivingtour.entity.ERoute;

public class DataService {
	private final static String TAG = DataService.class.getName();
	public MainDatabaseHelper dbOpenHelper;

	public DataService(Context context) {
		dbOpenHelper = new MainDatabaseHelper(context);
	}

	public List<ERoute> getList() {
		SQLiteDatabase database = dbOpenHelper.getReadableDatabase();
		List<ERoute> list = new ArrayList<ERoute>();
		Cursor cursor = database.rawQuery("select * from Route", null);
		while (cursor.moveToNext()) {
			list.add(new ERoute(cursor.getString(cursor.getColumnIndex("title")),
					cursor.getString(cursor.getColumnIndex("snippet")),
					cursor.getString(cursor.getColumnIndex("description")),
					cursor.getInt(cursor.getColumnIndex("startX")),
					cursor.getInt(cursor.getColumnIndex("startY")),
					cursor.getInt(cursor.getColumnIndex("endX")),
					cursor.getInt(cursor.getColumnIndex("endY")),
					cursor.getString(cursor.getColumnIndex("points"))));
		}
		cursor.close();
		database.close();
		return list;
	}

	public List<ERoute> getConditionList(String condition) {
		SQLiteDatabase database = dbOpenHelper.getReadableDatabase();
		List<ERoute> list = new ArrayList<ERoute>();
		Cursor cursor = database.rawQuery("select * from Route where title like '%" + condition + "%' ", null);
		while (cursor.moveToNext()) {
			list.add(new ERoute(cursor.getString(cursor.getColumnIndex("title")),
					cursor.getString(cursor.getColumnIndex("snippet")),
					cursor.getString(cursor.getColumnIndex("description")),
					cursor.getInt(cursor.getColumnIndex("startX")),
					cursor.getInt(cursor.getColumnIndex("startY")),
					cursor.getInt(cursor.getColumnIndex("endX")),
					cursor.getInt(cursor.getColumnIndex("endY")),
					cursor.getString(cursor.getColumnIndex("points"))));
		}
		cursor.close();
		database.close();
		return list;
	}

	public List<ELocation> searchNearLocations(GeoPoint p, int range, Integer[] types) {
		SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
		String sql = "SELECT ";
		for (String column : ELocation.Columns) {
			sql += column + ", ";
		}
		sql = sql.substring(0, sql.length() - 2);

		sql += " FROM Location where " +
				"abs(pointX-" + p.getLatitudeE6() + ")<" + range + " and abs(pointY-" + p.getLongitudeE6() + ")<"
				+ range;
		if (types != null && types.length > 0) {
			sql += " and (1=2 ";
			for (int t : types) {
				sql += " or type = " + t;
			}
			sql += ")";
		}
		Log.d(TAG, sql);
		List<ELocation> locations;
		Cursor c = null;
		locations = new ArrayList<ELocation>();
		try {
			c = db.rawQuery(sql, new String[] {});
			if (c != null) {
				for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
					ELocation vo = new ELocation();
					vo.fillEntity(c);
					locations.add(vo);
				}
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return locations;
	}
}
