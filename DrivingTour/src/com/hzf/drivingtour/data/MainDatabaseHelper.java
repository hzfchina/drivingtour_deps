package com.hzf.drivingtour.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.hzf.drivingtour.entity.AEntityBase;
import com.hzf.drivingtour.entity.ELocation;
import com.hzf.drivingtour.entity.ERoute;
import com.hzf.util.StrUtil;

public class MainDatabaseHelper extends SQLiteOpenHelper {
	public static final String TAG = MainDatabaseHelper.class.getName();
	private static final String DATABASE_NAME = "maindb";
	private static final int DATABASE_VERSION = 1;

	public MainDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + ERoute.Table_Name + "(" +
				" _ID INTEGER PRIMARY KEY, " +
				" title TEXT," +
				" snippet TEXT," +
				" description TEXT," +
				" startX INTEGER, " +
				" startY INTEGER, " +
				" endX INTEGER, " +
				" endY INTEGER, " +
				" points TEXT);");
		db.execSQL("CREATE TABLE " + ELocation.Table_Name + "(" +
				" _ID INTEGER PRIMARY KEY, " +
				" type INTEGER," +
				" title TEXT," +
				" snippet TEXT," +
				" description TEXT," +
				" pointX INTEGER, " +
				" pointY INTEGER);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + ERoute.Table_Name);
		db.execSQL("DROP TABLE IF EXISTS " + ELocation.Table_Name);
		onCreate(db);
	}

	public static void saveEntity(SQLiteDatabase db, AEntityBase vo) {
		String Table_Name = vo.getTable_Name();
		String[] Columns = vo.getColumns();
		String sql = "";
		List<Object> params = new ArrayList<Object>();
		if (vo.get_ID() != -1) {
			for (int i = 1; i < Columns.length; i++) {
				sql += " " + Columns[i] + "=?,";
				params.add(vo.val(i));
			}
			sql = StrUtil.removePostfix(sql, ",");
			sql += "update " + Table_Name + " set " + sql + " where _ID=?";
			params.add(vo.get_ID());
		} else {
			String values = "";
			sql = "insert into " + Table_Name + " (";
			for (int i = 1; i < Columns.length; i++) {
				sql += Columns[i] + ",";
				values += "?,";
				params.add(vo.val(i));
			}
			sql = StrUtil.removePostfix(sql, ",");
			values = StrUtil.removePostfix(values, ",");
			sql += ")values(" + values + ")";
		}
		Log.d(TAG, sql + "--" + params);
		db.execSQL(sql, params.toArray(new Object[] {}));
	}
}
