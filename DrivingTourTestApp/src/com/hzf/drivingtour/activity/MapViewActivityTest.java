package com.hzf.drivingtour.activity;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import android.annotation.TargetApi;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.Suppress;
import android.util.Log;

import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapView;
import com.hzf.drivingtour.DrivingTourApp;
import com.hzf.drivingtour.R;
import com.hzf.drivingtour.data.MainDatabaseHelper;
import com.hzf.drivingtour.entity.ELocation;
import com.hzf.drivingtour.entity.ERoute;
import com.hzf.drivingtour.map.MapUtil;
import com.hzf.drivingtour.map.SimpleItemizedOverlay;
import com.jayway.android.robotium.solo.Solo;

@TargetApi(8)
public class MapViewActivityTest extends
		ActivityInstrumentationTestCase2<MapViewActivity> {
	public static final String TAG = MapViewActivityTest.class.getName();
	private Solo solo = null;
	private MapViewActivity activity = null;
	private MapView mapView = null;
	private DrivingTourApp app = null;
	private MainDatabaseHelper dbHelper = null;

	public MapViewActivityTest() {
		super(MapViewActivity.class);
	}

	@Override
	protected void setUp() {
		try {
			super.setUp();
			solo = new Solo(getInstrumentation(), getActivity());
			activity = (MapViewActivity) solo.getCurrentActivity();
			mapView = (MapView) solo.getView(R.id.bmapView);
			app = (DrivingTourApp) activity.getApplication();
		} catch (Exception e) {
		}
	}

	@Override
	protected void tearDown() {
		try {
			activity.finish();
			super.tearDown();
			if (dbHelper != null) {
				dbHelper.close();
			}
		} catch (Exception e) {
		}
	}

	@Suppress
	public void testEntity() {
		assertEquals(ELocation.Columns.length, ELocation.Column_Types.length);
		assertEquals(ERoute.Columns.length, ERoute.Column_Types.length);
	}

	@Suppress
	public void testActivity() {
		try {
			solo.assertCurrentActivity(getName(), MapViewActivity.class);
			assertNotNull(mapView);
			assertNotNull(app);
			assertEquals("任我行", activity.getString(R.string.app_name));

			//地图上描点
			activity.clearAllOverlay();
			activity.addLocation(new ELocation(1, "Start", "起点", "", 102669277, 25067217));
			activity.addLocation(new ELocation(0, "科技孵化园1", "科技孵化园", "", 102781606, 24983819));
			activity.addLocation(new ELocation(2, "End", "终点", "", 102782088, 24983731));
			SystemClock.sleep(3000);
			assertEquals(MapViewActivity.ItemizedOverlayTypes.length, 18);
			assertNotNull(mapView.getOverlays());
			//			assertEquals(mapView.getOverlays().size(), MapViewActivity.ItemizedOverlayTypes.length);
			assertTrue(mapView.getOverlays().get(0) instanceof SimpleItemizedOverlay);

			//显示当前位置
			//			activity.addMyLocationOverlay();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
	}

	//	@Suppress
	public void testImportKML() {
		try {
			dbHelper = new MainDatabaseHelper(getActivity());
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			//reset all DB data
			dbHelper.onUpgrade(db, 0, 0);

			//import route1.kml into Route table
			InputStream is = getActivity().getAssets().open("route1.kml");
			ERoute route = MapUtil.parseKML(is);
			assertNotNull(route);
			assertEquals(route.getTitle(), "上班路线");
			assertTrue(route.getDescription() != null && route.getDescription().trim().length() > 0);
			assertEquals(route.getStartX(), 25067217);
			assertEquals(route.getStartY(), 102669277);
			assertEquals(route.getEndX(), 24983731);
			assertEquals(route.getEndY(), 102782088);
			assertTrue(route.getPoints() != null && route.getPoints().trim().length() > 0);
			MainDatabaseHelper.saveEntity(db, route);

			//import all Rotue points into Location table for test
			ELocation location = new ELocation();
			location.setTitle(route.getTitle() + "-起点");
			location.setGeoPoint(route.getStartGeoPoint());
			MainDatabaseHelper.saveEntity(db, location);

			location = new ELocation();
			location.setTitle(route.getTitle() + "-终点");
			location.setGeoPoint(route.getEndGeoPoint());
			MainDatabaseHelper.saveEntity(db, location);
			String[] ponits = route.getPoints().split(",");
			for (int i = 0; i < ponits.length; i += 8) {
				location = new ELocation();
				location.setType((i / 8) % 4);
				location.setTitle(route.getTitle() + "--" + i);
				location.setPointX(Integer.parseInt(ponits[i]));
				location.setPointY(Integer.parseInt(ponits[i + 1]));
				MainDatabaseHelper.saveEntity(db, location);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		} finally {
			if (dbHelper != null) {
				dbHelper.close();
			}
		}
	}

	public void testSearchNearLocations() {
		try {
			dbHelper = new MainDatabaseHelper(getActivity());
			GeoPoint p = new GeoPoint(25067217, 102669277);
			Integer[] types = new Integer[] { ELocation.Type_UNKNOWN, ELocation.Type_GasStation, ELocation.Type_Hotle };
			assertEquals(ELocation.Column_Types.length, ELocation.Columns.length);
			assertEquals(new ELocation().getColumn_Types().length, ELocation.Columns.length);

			List<ELocation> locations = DrivingTourApp.service.searchNearLocations(p, 20000, types);
			assertNotNull(locations);
			assertTrue(locations.size() > 0);
			for (ELocation l : locations) {
				Log.d(TAG, "distance=" + MapUtil.distance(p, l.getGeoPoint()));
				assertTrue(Arrays.binarySearch(types, l.getType()) >= 0);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		} finally {
			if (dbHelper != null) {
				dbHelper.close();
			}
		}
	}
}
